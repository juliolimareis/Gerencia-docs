# Documento de declaração de visão

### 1. Titulo
Website para Tec Toy

### 2. Objetivo 
Dentro de 60 dias, construir um Website para divugação dos produtos, serviços e parcerias 
da empresa Tec Toy, gastando no máximo R$ 30,000,00. 

### 3 Justificativa
Hoje em dia, toda empresa precisa ter um website moderno e bastante objetivo, falando sobre 
seus serviços, princípios, valores, equipe etc., feito sob medida para seu público alvo e 
deixando claro o seu posicionamento no mercado.

O atual site da Tec Toy está totalmente desatualizado quando às informações institucionais,
serviços e até mesmo em termos de layout/design, pois foi construído há uma década. Assim precisa ser 
refeito para que possa tanto atrair a audiência correta quanto também informa-la melhor sobre os produtos e 
serviços que a empresa tem a oferecer, geranso assim mais oportunidades de negócios.
 
### 4. Descrição Geral


### 8. Riscos Preliminares
